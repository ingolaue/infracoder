# Topology Definition

Topology files enable operators to introduce self-service for the deployment process of solutions that contain multiple server. A topology describes the set of components a service consists of, as well as their mutual dependencies. Topology definitions rely on artifacts to ensure interoperability between components of a service accross data centers and regions. While configuration files are developed in collaboration with developers who understands the internals of a particular application, topology definitions are created together with operators who analyze the requirements and understand the constraints of the systems- and communication architecture. Definition files map the specified service topology to available infrastructure. Publishing these files in a catalog allows business users to create instances of a service on demand.

## Using Terraform

We use Terraform to create service instances with HCL. HCL templates are not only descriptions of a service topology, but also build plans. Terraform runs build plans written in HCL and the  [service provider](https://www.terraform.io/docs/providers/oci/index.html) adapts the plan to the  infrastructure. With Terraform operators automate a lot of tasks, necessary to manage state changes throughout the lifecycle of a service. The tool helps to add, change or delete resources and services.  Managing build plans with the [OCI resource manager](https://docs.cloud.oracle.com/iaas/Content/ResourceManager/Concepts/resourcemanager.htm) reduces the hosting cost significantly by sharing reusable knowledge about best practices for managing client specific services. While HCL templates translate deep domain knowledge into a plan, the resource manger allows to use plans by simply “invoking” it.  

### Setup

In order to use Terraform we need to provide the access information for a particular compartment. Terraform handles secrets as variables. We keep secrets separated from the deployment code to avoid any exposure on GitLab by storing secrets in the [terraform.tfvars file](/topology/templates/tpl_terraform.tfvars) and specify this service in the .gitignore file. We create a terraform.tfvars file by coping the sources.pkr.hcl. 

```
cp ~/git/artifacts/packer/adminhost/sources.pkr.hcl ~/git/topology/terraform.tfvars && nano ~/git/topology/terraform.tfvars
```

We reuse the existing keys but adpat the structure mentioned above.

  
```
# --- Tenant Information ---
tenancy_ocid     = "ocid1.tenancy.xxx"
user_ocid        = "ocid1.user.xxx"
fingerprint      = " ... "
private_key_path = "~/.oci/oci_api_key.pem"
compartment_ocid = "ocid1.compartment.oc1.xxx"
region           = "us-phoenix-1"

# --- Authorized public IPs ingress (0.0.0.0/0 means all Internet) ---
authorized_ips  = "0.0.0.0/0" # all Internet


# --- variables for BM/VM creation ---
ssh_public_key_path  = "~/.ssh/id_rsa.pub"
ssh_private_key_path = "~/.ssh/id_rsa"

# --- shared state ---
aws_access_key  = " ..."
aws_secret_key  = " ... "
```

Fingerprint:
```
cat /home/torsten/.oci/oci_api_key_fingerprint
```

### Configure the state file location

```
mkdir ~/.aws/ && touch ~/.aws/credentials
```

Retrieve the name space

```
oci os ns get
```

URL
```
https://{namespace}.compat.objectstorage.{region}.oraclecloud.com
```


## Templates

Terraform uses the same HCL syntax as Packer, but in a much more flexible manor. Configuration and execution building blocks are used to describe components of a service. 

### Syntax

We store blocks in separate files to create modules, a self-contained configuration packages. Relying on modules helps to organize infrastructure code and to create reusable components. 
```
resource "<PROVIDER>_<TYPE>" "<NAME>" {
 [CONFIG …]
}
```


### Scripting Functionality

Even though HCL is a definition language, primitives allow to write scripts as part of a template. Terraform supports loops, conditionals, ternary operators and counts and provides some predefined expressions like create_before_destroy.

### OCI-Utils
OCI Utilities extend the bash scripting functionality of an Oracle Linux server on OCI. Using OCI utilas allows for the use of variables and other features intended to make topology files more versatile and reusable. We check how retrieve instance data using [oci-utils](https://docs.cloud.oracle.com/iaas/Content/Compute/References/ociutilities.htm)

```
oci-public-ip

oci-public-ip -g

oci-metadata
```

The oci-public-ip command is installed with the OCI Utilities. It allows to request the host IP from the cloud controller. Option "-g" reduces the output to the actual IP address. oci-metadata provides details about your instance
Next we use the SED commend to adjust the index.html page. 

```
myip=`oci-public-ip -g` && sudo sed -i "s;_server-information_;$myip;" index.html
```

The initial Terraform code is based on the following definition files:

## File Structure

```
topology
├── main.tf
├── provider.tf
├── vars.tf
├── datasources.tf
└── terraform.tfvars
```

### Main

```
module "network" {
  region = "us-phoenix-1"
  ...
}
```

### Provider

```
provider "oci" {
  region = "us-phoenix-1"
}
```

### Variables

```variable "NAME" {
 [CONFIG ...]
}
```

### Datasources

```
data "terraform_remote_state" "oci" {
  backend = "remote"

  config = {
    organization = "oracle"
    workspaces = {
      name = "dev"
    }
  }
}
```

## Directory Structure

```
topology
├── datastore
├── modules
├── network
└── services # can be Dev, Test or Prod

```

### Modules
...


## Build Process

The core Terraform workflow has the following steps:

* Initialize - Invoke the configuration settings for the deployment process
* Write - Author the initial or change an existing solution topology
* Validate - Check the written code for syntax errors
* Plan - Preview changes before applying
* Apply - Provision reproducible infrastructure


### Initialize

Terraform sources values in the terraform.tfvars file automatically. 

```
terraform init
```

### Write
...

### Validate
...
```
terraform validate
```

### Plan
...
```
TF_LOG=DEBUG OCI_GO_SDK_DEBUG=v terraform apply
```

### Apply

Deploy the AdminHost

To test whether the creation process was succesful, we launch an instance of the service. a respective [srcipt](artifacts/packer/CkptTest.sh) is in the artiface directory. We use oci-utils to collect the necessary information for the instantiation of an adminstrator host. Passing the JSON output through jq’s parser, allows to extract information from the next OCI command. 

## Reporting


## Misc
Once the compartment is created, the InfraCoder image requires configuration of secrets to access a particular tenant.

*  Create a [SSH key pair](https://docs.cloud.oracle.com/iaas/Content/GSG/Tasks/creatingkeys.htm) and a [API signing key](https://docs.cloud.oracle.com/iaas/Content/API/Concepts/apisigningkey.htm) in ~/.ssh 
*  Ensure that you have [S3 bucket access](https://docs.cloud.oracle.com/iaas/Content/Object/Tasks/s3compatibleapi.htm?Highlight=API%20Key%20pair) in your tenant, the directory for the credentials is ~/.aws
*  Prepare a [secrets.json](https://gitlab.com/de_cloud_platform_engineering/templates/blob/master/templates/secrets.json.template) and a secrets.tf template.


oci-utils is a bundle of tools that enable automation engineers to retrieve host information from the cloud controller, similar to the CLI. We add the [OCI-Utilities](https://docs.cloud.oracle.com/iaas/Content/Compute/References/ociutilities.htm)

[<<](packer.md) | [>>](...)