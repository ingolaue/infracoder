# Server Configuration

Building a service always starts with a first server. Shell scripting is the common technology to define services on a single machine. Scripts store a series of commands in a file and execute them together. With scripts repetitive tasks get automated. The shell can also take commands as input from a user or a file this allows to address changing conditions. We use bash, because these scripts can be excuted on all mayor operating systems incl. [Windows](https://docs.microsoft.com/en-us/windows/wsl/install-win10). 

## Syntax

Shell scripts have syntax just like any other programming language. However, in- and output arguments depend on the command. [Bash Scripting](https://devhints.io/bash) lists the syntax commands typically used for server configurations. 

```
command [OPTION]... [FILE]...
```

In addition, the [Shell Cheat Sheet](http://www.cheat-sheets.org/saved-copy/shellscripcheatsheet.pdf) provides an overview for configruation script constructs that comprise following elements. 

* Shell Keywords – if, else, break etc.
* Shell commands – cd, ls, echo, pwd, touch etc.
* Functions
* Control flow – if..then..else, case and shell loops etc.*

Constructs allow to build servers that serve as modules in a service oriented architecture. More detailed introductions into the different fileds of shell scripting can be found at the [Linux Foundation-](https://training.linuxfoundation.org/training/introduction-to-linux/), [Ryans Tutorials](https://ryanstutorials.net/bash-scripting-tutorial/) or [Oracle's university](https://www.oracle.com/technical-resources/articles/linux/saternos-scripting.html). This introduction only highlights functionality typically used configuration files and leads thorugh the typical use case, the process of defining a service that exposes a SSH terminal via HTTPS.  

## Setup a Local Server

For development purposes we start with a local machine. We use the InfraCoder image as base image. After importing the image to Virtual Box, it can be started in a terminal window.

```
VBoxManage startvm "InfraCoder"
```

The image starts headless and can be reached via ssh on the local IP *192.168.56.11*. The initial admin user is called *training* and the password *changeme*.

```
ssh training@192.168.56.11
```

When we login in for the first time, we need to complete the installation of the server by executing the predefined *setup.sh* script. It is in the home directory of the training user. The script downloads the installation procedure stored in a file called "admhst_ol77.sh" from GitLab. It updates the operating system, installs a couple of extensions and tools and creates a new admin user before it disables the 'training' user.

```
~/setup.sh
```

The following information will be requested:
* username: 
* password:
* configuration file: admhst_ol77.sh

The script will take a few minutes to complete. We need to log out when the script finishes. Logging in again, we can already use a browser. We recommend the Firefox browser for coders not familar with handling self-assigned certificates manually. 

> https://192.168.56.11/cockpit+app/@localhost/system/terminal.html

Note: No official CA will validate the self-signed certificat on the local InfraCoder instance. Requesting the ssh terminal for the first time the browser will show a security violation and request a confirmation, Chrome users need to perform a [manual import](https://www.accuweaver.com/2014/09/19/make-chrome-accept-a-self-signed-certificate-on-osx/) before confirming the request.

## Bootstrap a Service

The creation af a configuration file starts with the investigation of the core installation procedure for a service. In our example we install the cockpit application. The software is available in the Oracle linux EPEL repository, what makes the installation procedure pretty simple. We use Oracle Linux 7 as base image, which means that most of the Cent OS or Red Hat installations will work as well. E.g. for cockpit we can refer to the [CentOS](https://cockpit-project.org/running#centos) procedure on the project website.

```
# Install cockpit
sudo yum install cockpit

# Enable cockpit:
sudo systemctl enable --now cockpit.socket

# Open the firewall
sudo firewall-cmd --permanent --zone=public --add-service=cockpit
sudo firewall-cmd --reload
```

## Configuration File

With service definitions for cloud services can create single- or multi-purpose machines. Single purpose machines or "nodes" are usually managed as immutable, multi-purpose machines or "hosts" are managed mutable machines. This reflects the ablity to maintain and patch the system while it is running or configuration files that reside in a repository. Containers can be both, single- or multi-purpuses but are always treated as immutable artefacts. We define configuration files to automate the deployment of an instance. The definition starts from a base image, usually an operating system, installs additional packages and enables communication locally. 

### File Structure

Embedding the core installation procedure in a predefined [template](artifacts/bash/strct_ConfFile.sh) provides operators and service managers with the flexibility to work independent from each other towards a common base. We use a service templates with five building blocks. However, adopting this structure is a convention, that every administrator should refelct in the context of his own organizartion

```
#!/bin/bash

## --- Header ---
<Information regarding the service and the context of use>

## --- Variables ---
<Configuration Elements that are refered in the script>

## --- System Updates ---
<Updating and adding packages to the operating system>

## --- Admin User ---
<Creating a an admin user incl. home directory and bash profile>

## --- Software Installation ---
<Additonal install of software that is not maintained by the package manager>

## --- Enabling Communication ---
<Configuring communication interfaces, the firewall daemon and create SE Linux policies>
```

### Header

The template header contains the static meta-data describing the service and its context. We store header information as *comments*. In bash the *#* sign marks a line as comment. For header information we use the form *Identifier: Data*. The colon as delimiter allows to extract header informations with a streaming editor.

```
## --- Header ---
## Prepared for <organization>
## Service: <name>
## Description: <description>
## Based on: <URL to website>
## Base image: Oracle Linux 7.7
## Interface: <URL>
## Version: <0.0> 
## Date: <date>
## Author: <eMail>
## Tags: [...]
```

### Variables

Variables cache data that is used throughout the execution of a script. Using varaibles should reflect the context of a configuration file and allow operators to execute update and maintenance tasks without reading or changing the entire code of a script. Variable names can be any text string of up to 20 letters, digits, or an underscore character. A variable can represent various objects like an entry, a list of entries a path, etc. We distinguish enviromental and shell variables. **Environmental variables** are set for the active shell and inherited across scripts, while **shell variables** are contained exclusively within the script in which they are defined. Inside a script we distinguish global and local variables. Local variables span the execution of a function, global variables can be used accross functions within the same script. Defining a variable is simple using the 'equals' sign *variable=input*.

```
## --- Variables ---
# Environmental variables
export LANGUAGE=$(echo $LANG)
export LC_ALL=$(echo $LANG)

# Shell variables
TerraformRelease="0.12.18"
PackerRelease="1.5.1"

# Inputs
username=${1?Error: no name given}
password=${2?Error: no password given}

# Lists
PortList='22 80 443 990 1521'
```

### System Updates

After setting variables we prepare the operating system and inherit service functionality that can be sourced from the repositories of the image provider. In modern linux distributions the line between operating system and application code is blurring. Applications can be obtained as packages or as software with a separate installer. As cloud operator we prefer packages, package manager automatically resolve dependencies during the installation and allow to automate updates - a prerequisiste to work with immutable server. Using a commercial distribution and sourcing packages from the image provider ensures that the service is covered with a third-level support agreement. 

Preparing the operating system is done in three steps, clearing the yum cash, updating installed packages and adding packages that address the functional requirements. Running an update creating an instance corrects bugs and known vulnaribilities. Note: while 'update' only corrects installed packages, `yum -y upgrade` would also delete obsolete packages. But this can impact the functionality of installed applications and should therefore only be performed manually.

```
## --- System Updates ---
sudo yum clean all
sudo yum -y update
sudo yum -y install jq wget unzip nmap oci-utils <...>
```

### Admin User

Before installing additonal applications, we create a new admin user. While cloud server employ ssh keys to authorize admin users, local development server don't need that level of security. We employ **conditionals** to reflect this difference in the development process. Using conditionals enables engineers to depend the dynamic execution of tasks on inputs like a variable or an output from a command. In our case, we select different commands by checking the lifecycle stage of the image by verifying whether the "authorized_keys" file already exist. It is either copied or created. 

```
SSHScrt=/home/opc/.ssh/authorized_keys
if [ -f "$SSHScrt" ]; then
  sudo cp /home/opc/.ssh/authorized_keys /home/$username/.ssh/authorized_keys && sudo chown $username:$username /home/$username/.ssh/authorized_keys
else 
  sudo touch /home/$username/.ssh/authorized_keys && sudo chown $username:$username /home/$username/.ssh/authorized_keys
fi
```

A more complex example leverages conditionals to prevent data corruption by repeated executions of the same command. The id command reads entries in the user file. Adding the *-u* option retrieves the numeric user ID (UID):
> $ id -u
> 1001

It's a common Unix convention to assign the number zero as UID for root. Checking whether a UID equals '0' With *-eq 0* before executing the *useradd* command prohibits the creation of new users without root permissions and we can check whether an entry is valid by searching for a user in the passwd file.

```
## --- Admin User ---
if [ $(id -u) -eq 0 ]; then
	egrep "^$username" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo "$username exists!"
		exit 1
	else
		pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
		useradd -m -p $pass $username
		[ $? -eq 0 ] && echo -e "User has been added to system!" || echo -e "Failed to add a user!"
	fi
else
	echo "Only root may add a user to the system"
	exit 2
fi
```

### Software Installation

Another way to retrieve software for a server and perfrom an automated installation is wget. The tool is included in most linux distributions, it supports downloading via HTTP, HTTPS, and FTP. After the download, archives are decompressed and stored in the execution directory. While cockpit itself is delivered as package, we use manual procedures to install Packer and Terraform. For continuous deployments, applications obtained via public sources should be added to a private repository.

```
## --- Software Installation ---
cd /tmp
wget https://releases.hashicorp.com/terraform/$TerraformRelease/terraform_${TerraformRelease}_linux_amd64.zip
wget https://releases.hashicorp.com/packer/$PackerRelease/packer_${PackerRelease}_linux_amd64.zip
sudo unzip ./terraform_${TerraformRelease}_linux_amd64.zip -d /usr/local/bin/
sudo unzip packer_${PackerRelease}_linux_amd64.zip -d /usr/local/bin/
```

In order to customize configuration files we employ **text processing tools** like:

* [TR](https://www.howtoforge.com/linux-tr-command/) to work on characters, e.g. changing lower- to upper case letters
* [AWK](https://www.grymoire.com/Unix/Awk.html) to change records with fields identified by lines
* [SED](https://www.grymoire.com/Unix/Sed.html) to modify text snippets

Most often we rely on the **Streaming EDitor (SED)**. It parses and transforms text with a simple, compact programming language to search, find, replace, insert and/or delete text snippets. E.g. we use SED to substituting the number `443` with '9090' in the `cockpit.socket` file, which redirects the default listener and ensures transport layer security between the browser and the server.

```
sudo sed -i 's;'9090';'443';' /usr/lib/systemd/system/cockpit.socket
```

### Enabling Communication

The last step is ensuring communication on operating system level. We need to open the firewall and set rules for the SELinux framework. For the admin host we enable the following protocols on the firewall: *SSH, HTTPS* and *STFP*, extended default security list by opening the ports *21, 22, 80, 443* and *990*, consider to open port *5910-5911* for image creation with Packer and consider to enable *SQLNet* by opening port *1521*. **Loops** are typically used to repeat a task using different input values. Instead of creating five tasks for opening five ports on the firewall, we create a single task and use a loop to repeat the task with all the different ports that need to be opened.

```
PortList='22 80 443 990 1521'

## --- Enabling Communication ---
for val in $PortList;
    do
    sudo firewall-cmd --zone=public --add-port=$val/tcp --permanent
    sudo semanage port -m -t websm_port_t -p tcp $val
    done
```

## Example: Configuration File for an Administration Host

The [admhst_ol77.sh](templates/bash/admhst_ol77.sh) script is based on the Oracle Linux 7.7 template. OL77 is avaialble on OCI as service provider image or as "Autonomous Linux" image. The script creates the a jump host for projects in OCI. It automates the following procedure:

* Update the operating system, install required packages and set basic environment variables
* Secure access by creating a new, and disable ssh access for the default admin user
* Create home directory for the new admin user, set permissions and SSH policies in SELinux 
* Download software that is not covered by Oracle's EPEL license 
* Install the Infracoder toolset, the OCI command line interface and the cockpit repository
* Restart the services and replicate the InfraCoder git repository
  
```
#!/bin/bash

## --- Header ---
# Prepared for Oracle
# Service: Cockpit
# Description: Administratiion Host - expose ssh terminal to a browser via HTTPS
# Web: https://cockpit-project.org/
# Base image: Oracle Linux 7.7
# Interface: <URL>
# Version: 0.4 
# Date: 02/01/2020
# Author: torsten.boettjer@oracle.com
# Tags: [MGT]

## --- Variables ---
export LANGUAGE=$(echo $LANG)
export LC_ALL=$(echo $LANG)

TerraformRelease="0.12.18"
PackerRelease="1.5.1"

username=$(awk '{print $1}' /tmp/credentials.txt)
password=$(awk '{print $2}' /tmp/credentials.txt)

PortList='22 80 443 990 1521'

## --- System Updates ---
sudo yum clean all
sudo yum -y update
sudo yum -y install jq wget unzip cockpit policycoreutils-python oci-utils nmap

## --- Admin User ---
clear

if [ $(id -u) -eq 0 ]; then
	egrep "^$username" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo "$username exists!"
		exit 1
	else
		pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
		useradd -m -p $pass $username
		[ $? -eq 0 ] && echo -e "User has been added to system!" || echo -e "Failed to add a user!"
	fi

    sudo usermod -aG wheel $username
    mkdir /home/$username/.ssh && sudo chown $username:$username /home/$username/.ssh/
    
    SSHScrt=/home/opc/.ssh/authorized_keys
    if [ -f "$SSHScrt" ]; then
      sudo cp /home/opc/.ssh/authorized_keys /home/$username/.ssh/authorized_keys && sudo chown $username:$username /home/$username/.ssh/authorized_keys
    else 
      sudo touch /home/$username/.ssh/authorized_keys && sudo chown $username:$username /home/$username/.ssh/authorized_keys
    fi

    sudo chmod 700 /home/$username/.ssh/ && sudo chmod 600 /home/$username/.ssh/authorized_keys
    sudo echo "AllowUsers $username" >> /etc/ssh/sshd_config
    sudo restorecon -R -v /home/$username/.ssh

    # Install CLI "PATH=$PATH:</home/$username/bin/oci>" is appended to /home/$username/.bashrc
    cd /home/$username
    wget https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh 
    sudo chmod +x install.sh
    sudo chown $username:$username /home/$username/install.sh 
    sudo -H -u $username bash -c './install.sh --accept-all-defaults'

    # Install git and clone infracoder repository
    sudo yum -y install git
    sudo git clone https://gitlab.com/tboettjer/infracoder.git /home/$username/infracoder/
    sudo chown $username:$username /home/$username/infracoder/
    sudo rm -r /home/$username/infracoder/.git && sudo rm -r /home/$username/infracoder/docs
    sudo rm /home/$username/infracoder/*.md && sudo rm /home/$username/infracoder/.gitignore
    touch /home/$username/infracoder/README.md
    sudo chown -R $username:$username /home/$username/infracoder 
else
	echo "Only root may add a user to the system"
	exit 2
fi

## --- Software Installation ---
cd /tmp
wget https://releases.hashicorp.com/terraform/$TerraformRelease/terraform_${TerraformRelease}_linux_amd64.zip
wget https://releases.hashicorp.com/packer/$PackerRelease/packer_${PackerRelease}_linux_amd64.zip
sudo unzip ./terraform_${TerraformRelease}_linux_amd64.zip -d /usr/local/bin/
sudo unzip packer_${PackerRelease}_linux_amd64.zip -d /usr/local/bin/

## --- Enabling Communication ---
for val in $PortList;
  do
    sudo firewall-cmd --zone=public --add-port=$val/tcp --permanent
    sudo semanage port -m -t websm_port_t -p tcp $val
  done
sudo firewall-cmd --reload

# Start and enable cockpit
sudo sed -i 's;'9090';'443';' /usr/lib/systemd/system/cockpit.socket
sudo systemctl enable --now cockpit.socket

## --- Housekeeping ---
# Clean directories
setupfile=`basename "$0"`
sudo rm $HOME/$setupfile
sudo rm /tmp/credentials.txt

# Delete default user 'training'
if id -u "training" >/dev/null 2>&1; then
  sudo userdel -Z -f -r training
  clear
  echo "installation completed, please logout, open the browser and login with the new user"
else
  clear
  echo "installation completed, please logout, open the browser and login with the new user"
fi
```

Eventhough the script is written for OCI, it can be executed on the InfrasCoder image. Locally the web terminal is accessed through a browser using the local [IP](https://192.168.56.11:443/cockpit+app/@localhost/system/terminal.html). After successfull installation, the browser shows the login page for the cockpit app. 

[<img src="docs/cockpit.png">](https://192.168.56.11/cockpit+app/@localhost/system/terminal.html)

When this page shows up, the development server is correctly installed.  

[<<](README.md) | [>>](git.md)