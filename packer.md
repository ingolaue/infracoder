# Creating Custom Images

A deployment artifact is an image or a container that can be launched without further compilation, built, bundling, or code optimization. Images are managed in the image store and container in the OCI registry. Both artifacts are precompiled and prepackaged. The aim is to download artifacts as fast as possible onto a server and run them immediately and to minimize no service interruption. In addtion to that artifacts streamline the dev and test worklflow, by deploying the same artiface to staging and production servers. 

## Packer Templates

[Packer](https://www.packer.io/) is an open source tool for creating machine images and container. With Packer we store and version artifacts without persisting the binary code. Templates define how the operating system is merged with application software. Using templates allows operators to maintain artifacts distributed accross regions automatically. We should mention that another option to build custom images is exporting [OCI artifacts from VirtualBox](https://www.ateam-oracle.com/to-oci-and-back-with-virtualbox). however that is a manual process.

### Syntax

For the creation of custom images we employ Hashicorp's Configuration Language (HCL). The language was developed to define templates that are human and machine readable. HCL provides the code for deployment tools like Terraform and Packer. It combines JSON compatible expressions with scritping functionality. **Blocks** represent the configuration of a ressource in HCL. Every block is classified as a type, can carry multiple labels, and contains any number of arguments. Within blocks **arguments** assign expressions to an identifier, similar to local variables. **Expressions** are assigned with the syntax 'key = value'. The value can be any primitive: a string, number, boolean, object, or a list. Apart from that an argument can be defined in form of a nested block. 

```
<BLOCK TYPE> "<BLOCK LABEL>" "<BLOCK LABEL>" {
  <IDENTIFIER> = <EXPRESSION> # Argument
}
```

The complete HCL syntax is on [Github](https://github.com/hashicorp/hcl2/blob/master/hcl/hclsyntax/spec.md), images only use an [extract](https://www.packer.io/docs/configuration/from-1.5/syntax.html). We can limit the process of creating artifacts to the following [expressions](https://www.packer.io/docs/configuration/from-1.5/expressions.html).

* *Strings* are double-quoted and can contain any UTF-8 characters. Example: "us-phoenix-1"
* *Numbers* are assumed to be base 10
* *Boolean* values are true and false
* *Arrays* can be made by wrapping it in []. Example: [22, 80, 443]. Arrays can contain primitives, other arrays, and objects.

HCL also supports **Comments**, what makes it easy to navigate the code. Text can be stored in single- and multi-line comments.
* *Single line comments* start with # or //
* *Multi-line comments* are wrapped in /* and */. Nested block comments are not allowed. A multi-line comment (also known as a block comment) terminates at the first */ found.

### File Structure 

Definition files contain two major blocks, the sources and the build block. While the build block comprises the declarative code, used to create artifacts, the sources block contains the secrets to access an OCI tenant. 


```
source "oracle-oci" "phoenix" {
  region = "us-phoenix-1"
  ...
}

build {
  sources = ["source.oracle-oci.phoenix"]

  provisioner "shell" {
    inline = [
      "sleep 3",
      "sudo -u root echo 'AdminHost' > .ImageID"
    ]
  }
  ...
}
```

In our example we employ Packer to create a custom image for an administration host. The definition file is stored in the Packer directory, we create a [adminhost.pkr.hcl](https://gitlab.com/tboettjer/infracoder/raw/master/artifacts/packer/adminhost.pkr.hcl). As configuration file we continue to use the VirtualBox [script](artifacrts/../artifacts/bash/admhst_ol77.sh). 

```
cd ~/infracoder/artifacts/packer/ && cat adminhost.pkr.hcl
```

If the file is missing, we can retrieve it from the GitLab repository
```
wget [~/infracoder/artifacts/packer/template_secrets.json ~/infracoder/artifacts/packer/secrets.json](https://gitlab.com/tboettjer/infracoder/raw/master/artifacts/packer/adminhost_template.pkr.hcl)
```

A custom image is a regional resource, defining multiple "sources" in Packer enables engineers to store the same image in multiple regions. Custom images receive a unique identifier. We create a manifest file to capture the idnetifier together with the meta data for the image. 

### Sources Block

With the sources block we specify the tenant that we use. Packer can work with multiple tenants simoultanously, we just have to add another "source". The sources block provides the input variables for build blocks. E.g. defining a source requires an API access token. The token definition is declared inside the sources block, while the build declaration uses the *sources = ["source.oracle-oci.phoenix"]* syntax. 

```
source "oracle-oci" "phoenix" {
    region = "us-phoenix-1"
    tenancy_ocid = "ocid1.tenancy.xxx"
    compartment_ocid = "ocid1.compartment.xxx"
    availability_domain = "xxxx:PHX-AD-1"
    subnet_ocid = "ocid1.subnet.xxx"
    shape = "VM.Standard2.1"
    base_image_ocid = "ocid1.image.oc1.phx.aaaaaaaactxf4lnfjj6itfnblee3g3uckamdyhqkwfid6wslesdxmlukqvpa"
    image_name = "AdminHost"
    user_ocid = "ocid1.user.xxx"
    fingerprint = "xxx"
    key_file = "~/.ssh/xxx"
    ssh_username = "opc"
}
```

### Build Block

[Build](https://www.packer.io/docs/builders/index.html) blocks are invoked as part of a build in order to create an images. A packer instance can comprise multiple builders, beside OCI there are builders for VirtualBox, VMware, and OCI-C. Calling a component generates a machine image for the respective platform.

* [Provisioners](https://www.packer.io/docs/provisioners/index.html) install and configure software on a running machine prior to that machine being turned into a static image. In our example we use a simple shell script, however any configuration management system like Chef or Puppet can be used as provisioners.
* [Post-processors](https://www.packer.io/docs/post-processors/index.html) take the result of a builder or another process to create a new artifact, e.g. an installation manifest.


In our example, we build an administration host that acts as remote controller for the OCI tenant and secure access to the admin host through a web interface. 

```
build {
  sources = ["source.oracle-oci.phoenix"]

  provisioner "shell" {
    inline = [
      "sleep 3",
      "sudo -u root echo 'AdminHost' > .ImageID"
    ]
  }
  provisioner "file" {
    source = "../bash/admhst_ol77.sh"
    destination = "/tmp/admhst_ol77.sh"
  }

  provisioner "shell" {
    inline = [
      "echo 'changeme' | sudo chmod +x /tmp/admhst_ol77.sh",
      "sudo /tmp/admhst_ol77.sh var.AdminUser var.AdminPass"
    ]
  }

  post-processor "manifest" {
    output = "manifest.json"
   }
}
```

### Handling Secrets

Secrets are identifier, passwords, tokens, licenses, etc.. Commiting to GitLab, a public service, we avoid the sources block to become public-facing by only replacting code that is free of secrets. Initially we enable a secure sharing of definition files in a central repository by splitting the source from the build block and indicate the the file with the OCI secrets in the *.gitignore* file. The 'sercrets*' entry prevents the file from being synchronized. In a later stage we consider using Vault and the OCI Key Management Service.

## Build Process

The build process creates a custom image by taking the source blocks and and the build block to launch a server, create a custom image and retire the instance after completion. The post processor can be invoked to create a manifest. The build process is initiated by calling 'packer build' with a template file or a directory containing multiple *.pkr.hcl files. This process typically takes a few minutes. At the end Packer outputs the artifacts that have been created in JSON format. On OCI the file presents an OCID for every image.

```
cd ~/infracoder/artifacts/packer/ && packer build ~/infracoder/artifacts/packer/adminhost
```

When the build is complete, you will find the image in the image store of your tenant, the instance that was used by packer is automatically terminated after the build process. 

### Testing

We test whether the creation process was succesful by retrieving the OCID for the image.

```
cmprt=$(oci iam compartment list --all --query 'data[].{Name: "name", OCID: "id"}' | jq -r '.[] | select(.Name=="MGT") | .OCID')
img=$(oci compute image list -c $cmprt | jq -r '.data[] | select(."display-name"=="AdminHost") | .id')
echo $img
```

## Launch the Remote Administration Server

After creating a management network we are ready to launch a remote management server. We use the same AdminHost script that we have been using locally. We invoke a [script](artifacts/bash/admhst_launch.sh) to launch an instance of the Remote Administration Server. The necessary information is collected via OCI-utils.

```
#!/bin/bash

## --- Header ---
# Prepared for Oracle
# Service: SSH via HTTPS
# Description: The Remote Administration Host allows to access an SSH terminal through a browser
# Web: https://cockpit-project.org/
# Base image: Oracle Linux 7.7
# Interface: UI, https, Port 443
# Version: 0.1 
# Date: 02/01/2020
# Author: torsten.boettjer@oracle.com
# Tags: [MGT, Admin, VCN]

## --- Variables ---
cmprt=$(oci iam compartment list --all --query 'data[].{Name: "name", OCID: "id"}' | jq -r '.[] | select(.Name=="MGT") | .OCID')
vcn=$(oci network vcn list -c $cmprt | jq -r '.data[] | select(."display-name"=="MGT") | .id')
sbnt=$(oci network subnet list -c $cmprt --vcn-id $vcn | jq -r '.data[] | select(."display-name"=="MGT_ADMN") | .id')
ad=$(oci iam availability-domain list -c $cmprt | jq -r '.data[0] | .name')
img=$(oci compute image list -c $cmprt | jq -r '.data[] | select(."display-name"=="AdminHost") | .id')
shp="VM.Standard2.1"
sshkey=$(cat "$HOME/.ssh/id_rsa.pub")

## --- Launch Server Instance ---
oci compute instance launch -c $cmprt --availability-domain $ad --shape $shp --display-name "AdminHost" --image-id $img --subnet-id $sbnt --assign-public-ip true --metadata "{\"ssh_authorized_keys\": \"$sshkey\"}"

## --- Retrieve Public IP ---
clear && echo "This will take a moment ..."
sleep 2m
admhst=$(oci compute instance list -c $cmprt | jq -r '.data[] | select(."display-name"=="AdminHost") | select(."lifecycle-state"=="RUNNING") | .id')
ip=$(oci compute instance list-vnics --instance-id $admhst | jq -r '.data[] | ."public-ip"')

## --- Output ---
echo "$(echo 'https://')" $ip "$(echo '/')"
```

The script finishes with the IP for the server instance, we can copy the URL and paste it in a browser.

[<<](cli.md) | [>>](terraform.md)