# ---- use variables defined in terraform.tfvars file

variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key_path" {}
variable "compartment_ocid" {}
variable "region" {}
variable "ssh_public_key_path" {}
variable "ssh_private_key_path" {}
variable "authorized_ips" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}

# --- provider oci ---
provider "oci" {
  region = "us-phoenix-1"
  tenancy_ocid = var.tenancy_ocid
  user_ocid = var.user_ocid
  fingerprint = var.fingerprint
  private_key_path = var.private_key_path
  }

# --- compatibility ---
terraform {
  required_version = "> 0.12.16"
}