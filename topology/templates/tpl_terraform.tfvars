# --- Tenant Information ---
tenancy_ocid     = "ocid1.tenancy.xxx"
user_ocid        = "ocid1.user.xxx"
fingerprint      = " ... "
private_key_path = "~/.oci/oci_api_key.pem"
compartment_ocid = "ocid1.compartment.oc1.xxx"
region           = "us-phoenix-1"

# --- Authorized public IPs ingress (0.0.0.0/0 means all Internet) ---
# authorized_ips = "90.119.77.177/32" # a specific public IP on Internet
# authorized_ips = "129.156.0.0/16" # a specific Class B network on Internet
authorized_ips  = "0.0.0.0/0" # all Internet


# --- variables for BM/VM creation ---
ssh_public_key_path  = "~/.ssh/id_rsa.pub"
ssh_private_key_path = "~/.ssh/id_rsa"

# --- shared state ---
aws_access_key  = " ..."
aws_secret_key  = " ... "