module "user"{
    source = "./modules/identity/user"
    tenancy_ocid = "${var.tenancy_ocid}"
}

module "group"{
    source = "./modules/identity/group"
    tenancy_ocid = "${var.tenancy_ocid}"
    user_id = "${module.user.user_id}"
}

module "compartment"{
    source = "./modules/identity/compartment"
    tenancy_ocid = "${var.tenancy_ocid}"
    group_name = "${module.group.group_name}"
}

module "networking" {
  source = "./modules/networking"
  tenancy_ocid = "${var.tenancy_ocid}"
  compartment_ocid = "${module.compartment.compartment_id}"
  availability_domain = "${var.availability_domain}"
}

module "instance"{
    source = "./modules/compute/instance"
    tenancy_ocid = "${var.tenancy_ocid}"
    compartment_ocid = "${module.compartment.compartment_id}"
    availability_domain = "${var.availability_domain}"
    image_ocid = "${var.image_ocid}"
    instance_shape = "${var.instance_shape}"
    ssh_public_key = "${var.ssh_public_key}"
    subnet_ad1_ocid = "${module.networking.subnet_ad1_ocid}"
    subnet_ad2_ocid = "${module.networking.subnet_ad2_ocid}"
}
