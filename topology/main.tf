## --- Setup backend ---
terraform {
  backend "s3" {
    endpoint   = "https://tboettjer.compat.objectstorage.us-phoenix-1.oraclecloud.com"
    region     = "us-phoenix-1"
    bucket     = "tfstate"
    key        = "terraform.tfstate"
    shared_credentials_file = "~/.aws/credentials"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true
    skip_get_ec2_platforms      = true
    skip_metadata_api_check     = true
    force_path_style            = true
  }
}

## --- Call Network Module ---
module "network" {
  source            = "./network"
  compartment_ocid    = var.compartment_ocid
}