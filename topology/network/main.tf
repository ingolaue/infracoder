## --- Create a Virtual Cloud Network ---
resource "oci_core_vcn" "vcn" {
  cidr_block        = "10.0.0.0/16"
  compartment_id    = var.compartment_ocid
  display_name      = "ServiceVCN"
  dns_label         = "svcvcn"
}