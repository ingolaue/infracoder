// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

########################
# networking variables #
########################

variable "tenancy_ocid" {}
variable "compartment_ocid" {}
variable "availability_domain" {}