// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

###########
# subnets #
###########

output "subnet_ad1_ocid" {
  value = "${oci_core_subnet.subnet_ad1.id}"
}

output "subnet_ad2_ocid" {
  value = "${oci_core_subnet.subnet_ad2.id}"
}