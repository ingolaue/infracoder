// InfraCoder template for more information please go to https://gitlab.com/tboettjer/infracoder

## --- images ---
// See https://docs.cloud.oracle.com/iaas/images/
// Oracle-provided image "Oracle-Autonomous-Linux-7.7-2019.12-0"
variable "image_id" {
  type = map(string)
  default = {
    us-phoenix-1 = "ocid1.image.oc1.phx.aaaaaaaafmmoddld4cswpgjlkxikmluhoc4fpogjb23er65jjk4v3xtvdria"
    us-ashburn-1 = "ocid1.image.oc1.iad.aaaaaaaaib7kbfordqrrxkdq652nrpr5eego5pflpbzgtfjqrozt47yl6afq"
    eu-frankfurt-1 = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaazhlmuvqljodxke2mr2jwv63qiwbzjrawseiorthib45secauck2a"
    uk-london-1 = "ocid1.image.oc1.uk-london-1.aaaaaaaashzotj6muxv7kngmfojj4m7puurqovreapiqyzo7ezgt4cqfe3bq"
  }
}

## --- Configuration ---
variable "availability_domain" {
    default = 1
}

variable "image_ocid" {
    default = "ocid1.image.oc1.phx.aaaaaaaactxf4lnfjj6itfnblee3g3uckamdyhqkwfid6wslesdxmlukqvpa"
}

variable "instance_shape" {
    default = "VM.Standard2.1"
}

## --- Sizing ---
variable "instance_count" {
    default = 1
}