// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

############################################
# creating a group and adding a user to it #
############################################

resource "oci_identity_group" "default-group" {
  name           = "default-app-group"
  description    = "group created by terraform"
  compartment_id = "${var.tenancy_ocid}"
}

resource "oci_identity_user_group_membership" "user-group-mem1" {
  compartment_id = "${var.tenancy_ocid}"
  user_id        = "${var.user_id}"
  group_id       = "${oci_identity_group.default-group.id}"
}

data "oci_identity_groups" "groups1" {
  compartment_id = "${oci_identity_group.default-group.compartment_id}"

  filter {
    name   = "name"
    values = ["default-app-group"]
  }
}