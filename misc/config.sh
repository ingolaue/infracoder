#!/bin/bash

#Compartment OCID
read -e -p "Enter a compartment OCID: " -i "ocid1.tenancy.xxx" cmprt
export $cmprt

echo "-- create a compute instance from Oracle Linux 8 image"

# Set variables
export ad=$(oci iam availability-domain list -c $cmprt | jq --raw-output .data[0].name)
export ol8image="ocid1.image.oc1..aaaaaaaaiqhm2r5wsdd4p2dnz4s2q34nblvxwui45v6vpegpr2rjfeqb7r7q"
export shape="VM.Standard.E2.2"
export sshkey=$(cat "$HOME/.ssh/pub/id_rsa_router.pub")

oci compute instance launch -c $cmprt --availability-domain $ad --shape $shape --display-name "admhost" --image-id $ol8image --subnet-id $sbntmgmt --assign-public-ip true --metadata "{\"ssh_authorized_keys\": \"$sshkey\"}"