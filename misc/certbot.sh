#!/bin/bash

###################
# Install Certbot #
###################

wget https://dl.eff.org/certbot-auto
sudo mv certbot-auto /usr/local/bin/certbot-auto
sudo chown root /usr/local/bin/certbot-auto
sudo chmod 0755 /usr/local/bin/certbot-auto

######################
# OL8 bug workaround #
######################

sudo dnf install python3-virtualenv

#####################################
# get and install your certificates #
#####################################

sudo /usr/local/bin/certbot-auto --nginx

############################
# Set up automatic renewal #
############################

echo "0 0,12 * * * root python -c 'import random; import time; time.sleep(random.random() * 3600)' && /usr/local/bin/certbot-auto renew" | sudo tee -a /etc/crontab > /dev/null


#########################
# Secure cockpit access #
#########################

sudo dnf -y certmonger
hostname=$(mgt.ocilabs.io)

CERT_FILE=/etc/letsencrypt/live/mgt.ocilabs.iofullchain.pem
KEY_FILE=/etc/letsencrypt/live/mgt.ocilabs.io/privkey.pem

getcert request -f ${CERT_FILE} -k ${KEY_FILE} -D $(hostname --fqdn) -C "sed -n w/etc/cockpit/ws-certs.d/50-from-certmonger.cert ${CERT_FILE} ${KEY_FILE}"