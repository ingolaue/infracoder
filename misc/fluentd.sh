#!/bin/bash
sudo curl -L https://toolbelt.treasuredata.com/sh/install-redhat-td-agent3.sh | sh
sudo systemctl enable td-agent.service && sudo systemctl start td-agent.service
sudo chown -R root:td-agent /var/log
sudo chmod -R 775 /var/log/