
# -------- get the list of available ADs
data "oci_identity_availability_domains" "ADs" {
    compartment_id = "${var.tenancy_ocid}"
    }

# ------ Create a new VCN
variable "VCN-CIDR" { default = "10.0.0.0/16" }
resource "oci_core_virtual_network" "tf-demo01-vcn" {
    cidr_block = "${var.VCN-CIDR}"
    compartment_id = "${var.compartment_ocid}"
    display_name = "tf-demo01-vcn"
    dns_label = "tfdemovcn"
    }

# ------ Create a new Internet Gateway
resource "oci_core_internet_gateway" "tf-demo01-ig" {
    compartment_id = "${var.compartment_ocid}"
    display_name = "tf-demo01-internet-gateway"
    vcn_id = "${oci_core_virtual_network.tf-demo01-vcn.id}"
    }

# ------ Create a new Route Table
resource "oci_core_route_table" "tf-demo01-rt" {
    compartment_id = "${var.compartment_ocid}"
    vcn_id = "${oci_core_virtual_network.tf-demo01-vcn.id}"
    display_name = "tf-demo01-route-table"
    route_rules {
        cidr_block = "0.0.0.0/0"
        network_entity_id = "${oci_core_internet_gateway.tf-demo01-ig.id}"
        }
    }

# ------ Create a new security list to be used in the new subnet
resource "oci_core_security_list" "tf-demo01-subnet1-sl" {
    compartment_id = "${var.compartment_ocid}"
    display_name = "tf-demo01-subnet1-security-list"
    vcn_id = "${oci_core_virtual_network.tf-demo01-vcn.id}"
    egress_security_rules = [{
        protocol = "all"
        destination = "0.0.0.0/0"
        }]
    ingress_security_rules = [{
        protocol = "6" # tcp
        source = "${var.VCN-CIDR}"
        },
        {
        protocol = "6" # tcp
        source = "0.0.0.0/0"
        source = "${var.authorized_ips}"
        tcp_options {
            "min" = 22
            "max" = 22
            }
        }]
    }

# ------ Create a public subnet 1 in AD1 in the new VCN
resource "oci_core_subnet" "tf-demo01-public-subnet1" {
    availability_domain = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[var.AD - 1],"name")}"
    cidr_block = "10.0.1.0/24"
    display_name = "tf-demo01-public-subnet1"
    dns_label = "subnet1"
    compartment_id = "${var.compartment_ocid}"
    vcn_id = "${oci_core_virtual_network.tf-demo01-vcn.id}"
    route_table_id = "${oci_core_route_table.tf-demo01-rt.id}"
    security_list_ids = ["${oci_core_security_list.tf-demo01-subnet1-sl.id}"]
    dhcp_options_id = "${oci_core_virtual_network.tf-demo01-vcn.default_dhcp_options_id}"
}

# --------- Get the OCID for the more recent for Oracle Linux 7.4 disk image
data "oci_core_images" "OLImageOCID-ol7" {
    compartment_id = "${var.compartment_ocid}"
    operating_system = "Oracle Linux"
    operating_system_version = "7.4"
}

# ------ Create a compute instance from the more recent Oracle Linux 7.4 image
resource "oci_core_instance" "tf-demo01-ol7" {
    availability_domain = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[var.AD - 1],"name")}"
    compartment_id = "${var.compartment_ocid}"
    display_name = "tf-demo01-ol7"
    hostname_label = "tf-demo01-ol7"
    image = "${lookup(data.oci_core_images.OLImageOCID-ol7.images[0], "id")}"
    shape = "VM.Standard1.1"
    subnet_id = "${oci_core_subnet.tf-demo01-public-subnet1.id}"
    metadata {
        ssh_authorized_keys = "${file(var.ssh_public_key_file_ol7)}"
        user_data = "${base64encode(file(var.BootStrapFile_ol7))}"
    }
    timeouts {
        create = "30m"
    }
}

# ------ Display the public IP of instance
output " Public IP of instance " {
    value = ["${oci_core_instance.tf-demo01-ol7.public_ip}"]
}

# ------ Create a 500GB block volume
resource "oci_core_volume" "tf-demo01-ol7-vol1" {
    availability_domain = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[var.AD - 1],"name")}"
    compartment_id = "${var.compartment_ocid}"
    display_name = "tf-demo01-ol7-vol1"
    size_in_gbs = "500"
}

# ------ Attach the new block volume to the ol7 compute instance after it is created
resource "oci_core_volume_attachment" "tf-demo01-ol7-vol1-attach" {
    attachment_type = "iscsi"
    compartment_id = "${var.compartment_ocid}"
    instance_id = "${oci_core_instance.tf-demo01-ol7.id}"
    volume_id = "${oci_core_volume.tf-demo01-ol7-vol1.id}"
}