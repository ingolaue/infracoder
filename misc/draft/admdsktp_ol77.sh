
# OL 7.7 install
#
# Installing VNC Server + GUI 
# 1.) Install Steps

# as OPC user:
sudo yum install tigervnc-server
sudo yum groupinstall "server with gui"

# set VNC password for OPC user
vncpasswd

# copy VNC config file to allow for VNC Desktop 1 (:1)
sudo cp /lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@\:1.service


sudo vi /etc/systemd/system/vncserver@\:1.service
# replace <USER> with "opc"   // e.g. -> sed 's/<USER>/opc/g' 

# reload systemctl and start vncserver on desktop 1 (-> port 5901)
sudo systemctl daemon-reload
sudo systemctl start vncserver@\:1.service

# open system firewall to allow VNC
sudo firewall-cmd --zone=public --add-service=vnc-server --permanent





# Install noVNC 

# yum packages
sudo yum install novnc python-websockify numpy

# create certificates for HTTPs 
cd /etc/pki/tls/certs
sudo openssl req -x509 -nodes -newkey rsa:2048 -keyout /etc/pki/tls/certs/novnc.pem -out /etc/pki/tls/certs/novnc.pem -days 365

# start websockify and forward traffic to VNC port
websockify -D --web=/usr/share/novnc/ --cert=/etc/pki/tls/certs/novnc.pem 6080 localhost:5901




###################################################################

## Multi-User Operations:
## this requires repetition of above steps. Each user will have it's own vncserver running.

sudo cp /lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@\:2.service
sudo vi /etc/systemd/system/vncserver@\:2.service

# replace <USER> with required username (e.g. alex)
# e.g. 

sudo su - alex
vncpasswd alex

---

# start new vncserver running with user alex, on desktop :2 (port 5902) and exposed as 6082
sudo su - alex

# start 2nd vncserver for Alex
vncserver :2 
websockify -D --web=/usr/share/novnc/ --cert=/etc/pki/tls/certs/novnc.pem 6082 localhost:5902

# done.


