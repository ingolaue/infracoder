// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

# Secrets
variable "tenancy_ocid" {}
variable "user_ocid" {}
variable "fingerprint" {}
variable "image_ocid" {}
variable "private_key_path" {}
variable "region" {}
variable "ssh_public_key" {}
variable "ssh_private_key" {}

# Configuration
variable "availability_domain" {
  default="3"
}
variable "instance_user" {
  default="opc"
}
variable "region" {
  default="eu-frankfurt-1"
}
variable "image_ocid" {
  default="ocid1.image.oc1.eu-frankfurt-1.aaaaaaaatg3vbdctkxzvtzsrqhlkugep4rtmqohypphypbjjt34hib6i2jaa"
}
variable "instance_shape" {
  default="VM.Standard2.1"
}

# Sizing
variable "instance_count" {
  default="1"
}