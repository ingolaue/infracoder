#!/bin/bash

# --- set credentials ---
read -e -p "Create a new user: " username
read -e -p "Set a password: " password

# --- store credentials ---
touch /tmp/credentials.txt
echo $username $password >> /tmp/credentials.txt

# --- execute configuration script ---
wget https://gitlab.com/tboettjer/infracoder/raw/master/artefacts/bash/admhst_ol77.sh
echo 'changeme' | sudo -S chmod +x $HOME/admhst_ol77.sh
sudo ./admhst_ol77.sh $username $password