#!/bin/bash

######################################################################
# Git installation based on blog                                     #
# https://oracle-base.com/articles/linux/git-2-installation-on-linux #
######################################################################

sudo yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel -y
sudo yum install gcc perl-ExtUtils-MakeMaker -y
sudo yum remove git -y

cd /usr/src
sudo wget https://www.kernel.org/pub/software/scm/git/git-2.9.5.tar.gz
sudo tar xzf git-2.9.5.tar.gz

cd git-2.9.5
sudo make prefix=/usr/local/git all
sudo make prefix=/usr/local/git install
sudo chmod 777 /etc/bashrc
echo "export PATH=$PATH:/usr/local/git/bin" >> /etc/bashrc
sudo chmod 644 /etc/bashrc
source /etc/bashrc

git --version