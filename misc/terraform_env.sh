#!/bin/bash
#Tenancy OCID
read -e -p "Enter a tenancy OCID: " -i "ocid1.tenancy.xxx" TF_VAR_tenancy_ocid
export $TF_VAR_tenancy_ocid
#User OCID
read -e -p "Enter a user OCID: " -i "ocid1.user.xxx" TF_VAR_user_ocid
export $TF_VAR_user_ocid
#Fingerprint
read -e -p "Enter an API fingerprint: " -i "xx xx" TF_VAR_fingerprint
export $TF_VAR_fingerprint
#Path to Private API Key
read -e -p "Enter the API private key path: " -i "/home/training/.oci/oci_api_key_public.pem" TF_VAR_private_key_path
export $TF_VAR_private_key_path
#Path to Public SSH Key
export TF_VAR_ssh_public_key=$(cat /home/training/.ssh/infracoder.pub)
#Path to Private SSH Key
export TF_VAR_ssh_private_key=$(cat /home/training/.ssh/infracoder)