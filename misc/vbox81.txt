#!/bin/bash

# Use server image, create a user "training" with pwd "changeme"
# configure two network interfaces, NAT and Host-only Adapter
# Fixed disk size of 1 Core, 2048 MB, 64 GB 


export LANGUAGE=$(echo $LANG)
export LC_ALL=$(echo $LANG)

########################
# network interface up #
########################

sudo nmcli con mod enp0s8 ipv4.addresses 192.168.56.11/24
sudo nmcli con mod enp0s8 ipv4.gateway 192.168.56.1
sudo nmcli con mod enp0s8 ipv4.method manual
sudo nmcli con mod enp0s8 ipv4.dns "8.8.8.8"
sudo sed -i 's;ONBOOT=no;ONBOOT=yes;' /etc/sysconfig/network-scripts/ifcfg-enp0s8
sudo ifdown enp0s8 && sudo ifup enp0s8

#################################
# > VBoxManage startvm "GitOps" #
# install guest additions       #
#################################

# Insert Guest Additions

sudo dnf -y install kernel-devel-$(uname -r) kernel-headers perl gcc make elfutils-libelf-devel
sudo mkdir /media/cdrom
sudo mount /dev/cdrom /media/cdrom
sudo /media/cdrom/VBoxLinuxAdditions.run 


#############################################################
# > VBoxManage modifyvm "GitOps" --defaultfrontend headless #
#############################################################