# Bootstrap a Tenant

Bootstrap a tenant refers to the process of getting a tenant ready for building virtual datacenters. We create a management network that allows operators to deploy remote management tools or to host services that interface with the existing monitoring and managemement tools. In our example we create a vcn with a single subnet to host a remote console. The server is not meant to persist any data, it remains perishable and needs to be stopped when not in use in order to limit the risk exposure. 

## Command Line Interface (CLI)

The [CLI](https://docs.cloud.oracle.com/iaas/Content/API/Concepts/cliconcepts.htm) is an Oracle-maintained Python tool that extends the core functionality of the operating system on the desktop of the engineer. It is compatible with Windows, Mac, or Linux and can be used for scripting templating. CLI functionality is invoked with the 'oci' command.

### Syntax

The CLI is a management utility for OCI. With the oci command enigneers can create, resize, delete, change, copy and move resources in OCI. Most often it is used to query existing resources. The oci command supports regular expression to execute complex requests. 

```
oci [--option] <command> [<arguments>]
```
The [Hands-on-Lab](https://oracle.github.io/learning-library/oci-library/DevOps/OCI_CLI/OCI_CLI_HOL.html) is a good introduction for the CLI syntax.

### Using JSON Extracts

The execution of an oci command creates a resonse in JSON. Using the JSON parser [jq](https://stedolan.github.io/jq/) enables engineers to use this response for deployment automation. Tutorials like [reshaping JSON with jq](https://programminghistorian.org/en/lessons/json-and-jq) or [parse JSON data using jq](https://medium.com/how-tos-for-coders/https-medium-com-how-tos-for-coders-parse-json-data-using-jq-and-curl-from-command-line-5aa8a05cd79b) provide an introduction to JSON parsing. 

### CLI Setup

The CLI is preinstalled on the InfraCoder image, we only need to configure the service. The following information is required to use a particular tenant. To collect the necessary identifiers for the configuration files, we create a plain text file. We collect this information and store in a text file [SecretsTenant.txt](artifacts/bash/template_SecretsTenant.txt). Following is an example of my text file. The `secrets*` entry in th .gitignore file prevents this information from being synchronized with the git server. 

```
## --- Configuration ---
compartment=ocid1.xxx
tenancy="ocid1.tenancy.oc1..xxx"
region="us-phoenix-1"

## --- Identity ---
user="ocid1.user.oc1..xxx"
fingerprint=
  
## --- Public Key ---
key_file=/home/opc/.oci/oci_api_key.pem
```
   
The home region (e.g. "eu-frankfurt-1") is used for identification purposes only. It is not related to the location of the ressources that we deploy. We start the configuration by checking whether the CLI is correctly installed.

```
oci -v
```

The response should be a version number, like '2.7.0'. After that we configure the CLI using the data we just collected. We use a setup script that comes with the CLI.

```
oci setup config
```

We recommended to accept the default options for directories and to leave the passphrase empty. The configuration script finishes with the creation of the RSA key pair. This is an the API key, not a SSH key. The [documentation](https://docs.cloud.oracle.com/iaas/Content/API/Concepts/apisigningkey.htm#How2) describes the upload process

```
cat $HOME/.oci/oci_api_key_public.pem
```

* Open the Console
* Click your username in the top-right corner of the Console, and then click 'User Settings'
* Click Add Public Key and paste the contents of the PEM public key in the dialog box and click 'Add'

The tenant configuration and the key pair is stored at `/home/training/.oci`. Before we proceed, we load the public key and create a fingerprint that authorizes a user to call the API. The fingerprint is added through the web console. Before we proceed we check whether the API fingerprint was correctly created.

```
cat ~/.oci/config
```
Compare the finger print in the output of config file to the one in the web console window and make sure they match. 

## Define a Management Compartment

When the API key is loaded and API security is configured, we can test the connection to the OCI tenant by retrieving a list of the existing compartments. Compartments represents the root of an admin tree, like any other resource the management network needs to be associated with a [compartment](https://docs.cloud.oracle.com/iaas/Content/Identity/Tasks/managingcompartments.htm). We either create a new compartment or use an existing one. Compartment identifiers are globally unique and the number of compartments is limited, hence compartments should not be treated as perishable resource. 

```
oci iam compartment list
```

The CLI provides a reponse in JSON, which incl. all meta-data per compartment. For larger tenants this output becomes hard to read. Using the *table* option allows to format and the query.

```
oci iam compartment list --output table --all --query "data [*].{Name:\"name\", OCID:id}"
```

For an inital setup we either create a management compartment or use one that has been created before, e.g. by renaming a free compartment. In the beginn, a single management compartment is sufficient, but as most IT departments separate areas of competence like network- or database administration, contract external administrators and work with outsourcing partners to manage applications a more complex compartment structure should be considered in a later stage.

> cmprt=ocid1.compartment.xxx && oci iam compartment update -c $cmprt --name MGT

or

> oci iam compartment create --name MGT

Using OCI utils together with a parser helps engineers to script the deployment of the management network, incl. the creation of a new compartment, extracting the OCIDs and defining global variables for the active bash session.

```
export cmprt=$(oci iam compartment create --name MGT  | jq .data.id -r)
```

To reuse an existing compartment we have to add the compartment OCID that can be taken from the table, generated earlier.

```
export cmprt=$(oci iam compartment update -c ocid1.compartment.xxx --name MGT  | jq .data.id -r)
```


Both, the **create** and the **update** command confirms a successful execution with a JSON output that shows it's metadata. 

## Configuration File

The configuration file includes a sequence of commands defining the CIDR range, routing-, security-rules and an In  This includes assigning an own IP address space, creating subnets, route tables, and configuring stateful firewalls.ternet Gateway. Every command creates a JSON response including an own, unique identifier. We use 'jq' to extract the identifier and use it as input for the following commands. The [jq playground](https://jqplay.org/) helped to refine these filters.

### File Structure

The structure follows the outline for the configuration template introduced in the [bash section](bash.md).

```
#!/bin/bash

## --- Header ---
<Information regarding the service and the context of use>

## --- Variables ---
<Configuration Elements that are refered in the script>

## --- Virtual Cloud Network ---
<Create an own iP address space for management purposes>

## --- Administration Subnet ---
<Prepare the launch of management services in OCI>

## --- Artifact Subnet ---
<Carve out a small zone for the creation of artifacts>

## --- Enabling Communication ---
<Deploying an Internet-Gateway to provision public IP>
```

### Variables

The variables section for the bootstrap process refelcts the network configuration requirements. By default we allw egress traffic to any server and restrict ingress traffic to a minimum. In our example we only open the ports for SSH, HTTP and HTTPS.

```
## --- Variables ---
cmprt="ocid1.compartment.oc1..xxx"
location="us-phoenix-1"

# Ingress rules
portlist='22 80 443'
declare -a ingports

for port in $portlist; do
ingress () {
cat <<EOF
{"source": "0.0.0.0/0", "source-type": "CIDR_BLOCK", "protocol": 6, "isStateless": false, "tcp-options": {"destination-port-range": {"max": $port, "min": $port}}}
EOF
};
ingports+=$(ingress);
done

ingsecrules=$(echo ${ingports[@]} | jq . -s);

# Egress rules
egsecrule='{"destination": "0.0.0.0/0", "destination-type": "CIDR_BLOCK", "protocol": "all", "isStateless": false}'
```

### Virtual Cloud Network

The Virtual Cloud Network (VCN) represents a network domain including it's network functions. We can think of it as the root for any resource deployment. 

```
## --- Virtual Cloud Network ---
echo "-- create the MGT VCN"
export vcn=$(oci network vcn create --cidr-block 10.0.0.0/24 -c $cmprt --display-name MGT --dns-label mgt --region $location | jq --raw-output .data.id)
```

### Management Subnet

Within the VCN we reserve an address space for the deployment of management tools. Initially we only work with one IP address space, this can be extended later. However, the overall address space is limited by the size of the CIDR block defined in the VCN. Changing the size requires a rebuild of the network.

```
## --- MGT Subnet ---
echo "-- create ADM subnet and associated security list"
sl=$(oci network security-list create --display-name MGT_SL --vcn-id $vcn -c $cmprt --egress-security-rules "[ $egsecrule ]" --ingress-security-rules "$ingsecrules" | jq --compact-output [.data.id])
export sbntmgt=$(oci network subnet create --cidr-block 10.0.0.0/25 -c $cmprt --display-name ADM --vcn-id $vcn --security-list-ids $sl | jq --raw-output .data.id)
```

### Enabling Communication

After creating a network space we attach an internet gateway. Exposing servers to the internet requires a public IP address. While the VCN creates an internal address space the internetgateway is required to assign a public IP to a server. 

```
## --- Enabling Communication ---
echo "-- create the Internet Gateway then add a route rule to the default route table used by both subnets"
igw=$(oci network internet-gateway create -c $cmprt --is-enabled true --vcn-id $vcn --display-name MGT_IGW | jq --compact-output '. | [{cidrBlock:"0.0.0.0/0",networkEntityId: .data.id}]')
rt=$(oci network route-table list -c $cmprt --vcn-id $vcn | jq --raw-output '.data[].id')
oci network route-table update --rt-id $rt --route-rules $igw --force
```

## Executing the Script

The complete [bootstrap script](artifacts/bash/bootstrap.sh) can be found and executed in the InfraCoder repository.

```
sudo chmod +x ~/infracoder/artifacts/bash/bootstrap.sh
~/infracoder/artifacts/bash/bootstrap.sh
```

When the script finishes, it sends a JSON response with the details of the last command, which is the creation of a server. 

## Testing

The following command lists the server in the MGT compartment, we can identify the test server by its name "test"

```
oci compute instance list -c $cmprt --output table --all --query "data [*].{Name:\"display-name\", Region:\"region\", State:\"lifecycle-state\", OCID:\"id\"
```

Locate the public IP address of the instance using the CLI and feed it as URL in your browser.

```
export hst=$(oci compute instance list --display-name test -c $cmprt --lifecycle-state RUNNING | jq -r .data[].id)
export adr=$(oci compute instance list-vnics --instance-id $hst | grep public-ip | awk -F'[\"|\"]' '{print $4}')
nmap -p 80 $adr
```

The response should show the server up. 

> Starting Nmap 6.40 ( http://nmap.org ) at 2020-01-02 14:08 CET<br>
Nmap scan report for x.x.x.x<br>
Host is up (0.15s latency).<br>
PORT   STATE  SERVICE<br>
80/tcp closed http

When the server responses "up", we can terminate the test instance.

```
oci compute instance terminate --instance-id $hst
```

[<<](git.md) | [>>](packer.md)