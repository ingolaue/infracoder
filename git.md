# Code Sharing

Infrastructure coding is teamwork, sharing scripts privately and/or publically reduces administration efforts improves integrity over time. Cloud services for the Git version control system have evloved as de-facto standard for code sharing. We setup a git repository to manage and distribute definition files. 

## Command Syntax

Installing Git on a server extends the shell command set. The syntax is similar to unix user commands. [Pro Git](https://git-scm.com/book/en/v2) provides a comprehensive documentation of all git commands.

```
git [--option] <command> [<arguments>]
```

Sharing code only requires a small subset of the git functionality. The [Github Cheat Sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf) provides a comprehensive selection of the commands, typically used.


## Setup a Git Client

Git is already preinstalled on the InfraCoder image. With `git --version` we check whether git is correctly installed. The command should return a version number. In addition to that, we can familerize ourself with the basic syntax using 'help'. With **git help _verb_** helps to retrieve the documentation for a specific command. To use git, a user identity is required. This information is associated with code that we commit to a repository.

```
git config --global user.name "YourName"
git config --global user.email "your.name@oracle.com"
```

On most Unix systems *Vim* is configured as default editor, not necessarily the easiest program for beginners. Therefor we change the default setting and use *Nano* instead. 

```
git config --global core.editor nano
```

We check the current configuration, with the '--list' option. The changes, made above, should be reflected 

```
git config --list
```

## Start a Local Repository

There are two ways to initialise a git repository. `git clone` retrieves an existing project from a git server and creates a local repository and `git init` starts with creating a local directory. These commands create a hidden '.git' sub-directory that contains snapshots of the code changes. The InfraCoder image contains a directory '/home/$username/infracoder/' that we want to use as the InfraCoder repository.

```
cd ~/infracoder/ && git init
```

After initializing the repository, we specify a .gitignore file to prevent the syncronization of files that contain secrets. The [.gitignore file](.gitignore) in the root directory of the infracoder directory might be a good start. For more complex projects, GitHub maintains a fairly comprehensive list of good [.gitignore file examples](https://github.com/github/gitignore) for dozens of projects. 

Commiting code in git is a two step process. A staging step allows continue making changes in the working directory before commiting new code to version control. This allows to record smaller adjustments before publishing code changes to a broader audience. Initially we use the 'add .' command to transfers all existing data within the current directory to the staging area. The dot **.** refers to 'all'. 

```
git add .
```

With 'status' we check which files have been transfered to the staging area. The repsonse should show all files in the current directory

```
git status
```  

Now we can use *-commit* to create a snapshot and transfers the changes into the repository. The *-a* collects all changes made in the current directory and the *-m 'Initial Setup'* adds option adds the obligatory message helping engineers to track versions of the codebase.

```
git commit -a -m 'Initial Commit'
```

## Centralize Shared Repositories

The central git repository is the place for a team to pull code, contribute and reuse it. It enables the independent development of project components and prevents developers from blocking each other, because it helps to build independent project components. We start a repository by creating a local git directory on an InfraCoder instance and clone it on a GitLab server.

### Directory Structure

A formalized directory structure helps to organize infrastructure code in the repository. Adopting the outline as standard keeps the code clean and reusable. The InfraCoder image contains the "infracoder" directory as a blueprint for a repository structure.

```
.
├── README.md
├── docs
├── artifacts
│   ├── bash
│   │   ├── AdmHst_OL77.sh
│   │   └── bootstrap.sh
│   └── packer
│       ├── AdmHst_Test.sh
│       └── adminhost
│           ├── build.pkr.hcl
│           └── secrets.pkr.hcl
└── topology
    ├── main.tf
    ├── datastore
    │   ├── adw.tf
    │   ├── dbcs.tf
    │   ├── mysql.tf
    │   └── s3.tf
    ├── modules
    │   └── iam.tf
    ├── network
    │   ├── functions
    │   │   └── loadbalancer.tf
    │   └── main.tf
    └── services
        ├── backend
        └── frontend
```

### Choose a Service Model

There are multiple options to create and share a git repository: Public repositories are shared via [GitHub](https://www.github.com), multi-company teams can maintain private repositories on [GitLab](https://www.gitlab.com) and closed user groups setup a private [Git server](https://cloudmarketplace.oracle.com/marketplace/en_US/listing/37990057). InfraCoders at Oracle can request access to the *[InfraCoder repository on GitLab](https://gitlab.com/infracoding)* to receive a URL for the project.

As first step we generate a ssh key.

```
ssh-keygen -o -t rsa -b 4096 -C "your.name@oracle.com" -q -N "" -f ~/.ssh/id_rsa
```

With `cat ~/.ssh/id_rsa.pub` we show the public key, mark and copy it to the clipboard. To add the public SSH key to the GitLab account we open the web browser and navigate to the repository:

* Click the avatar in the upper right corner and select *Settings*.
* Navigating to SSH Keys and pasting the public key in the Key field. If you:
* Click the Add key button.

### Replicate the Code Base

Now we are ready to connect our local repository to GitLab and push the initial content

```
git remote add origin git@gitlab.com:infracoding/<project>.git
git push -u origin --all
```

The GitLab home directory should now look like this:

[<img src="docs/gitlab.png">](https://gitlab.com/infracoding)

###  Handle Differences

The main purpose of using a central repository is collaboration in a team. Hence, code changes are made locally and merged into a joined repository. It is good practice to start every coding session with a replication of the current content.

```
git pull
```

When two programmers work in paralell, consistency errors might occur the following commands help to automatically resolve such issues.

* **git log** - for finding specific commits by content of the messages or the difference 
* **git diff** - shows differences between development trees, e.g. between the working environment and the staging area, between staging area and last commit, or between two commits.

Another important feature of git is the management of different branches. However, InfraCoder teams are usually pretty small and code changes less frequent than in software projects, hence branches are sledmly needed.


[<<](bash.md) | [>>](cli.md)