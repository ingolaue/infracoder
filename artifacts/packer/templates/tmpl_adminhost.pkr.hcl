source "oracle-oci" "phoenix" {
    region = "us-phoenix-1"
    tenancy_ocid = "ocid1.tenancy.xxx"
    compartment_ocid = "ocid1.compartment.xxx"
    availability_domain = "xxxx:PHX-AD-1"
    subnet_ocid = "ocid1.subnet.xxx"
    shape = "VM.Standard2.1"
    base_image_ocid = "ocid1.image.oc1.phx.aaaaaaaactxf4lnfjj6itfnblee3g3uckamdyhqkwfid6wslesdxmlukqvpa"
    image_name = "AdminHost"
    user_ocid = "ocid1.user.xxx"
    fingerprint = "xxx"
    key_file = "~/.ssh/xxx"
    ssh_username = "opc"
}

build {
  sources = ["source.oracle-oci.phoenix"]

  provisioner "shell" {
    inline = [
      "sleep 3",
      "sudo -u root echo 'AdminHost' > .ImageID"
    ]
  }
  provisioner "file" {
    source = "../bash/admhst_ol77.sh"
    destination = "/tmp/admhst_ol77.sh"
  }

  provisioner "shell" {
    inline = [
      "echo 'changeme' | sudo chmod +x /tmp/admhst_ol77.sh",
      "sudo /tmp/admhst_ol77.sh var.AdminUser var.AdminPass"
    ]
  }

  post-processor "manifest" {
    output = "manifest.json"
   }
}