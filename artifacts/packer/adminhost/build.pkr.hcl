build {
  sources = ["source.oracle-oci.phoenix"]

  provisioner "shell" {
    inline = [
      "sleep 3",
      "sudo -u root echo 'AdminHost' > .ImageID"
    ]
  }
  provisioner "file" {
    source = "../../bash/admhst_ol77.sh"
    destination = "/tmp/admhst_ol77.sh"
  }
  provisioner "file" {
    source = "credentials.txt"
    destination = "/tmp/credentials.txt"
  }
  provisioner "shell" {
    inline = [
      "echo 'changeme' | sudo chmod +x /tmp/admhst_ol77.sh",
      "sudo /tmp/admhst_ol77.sh"
    ]
  }

  post-processor "manifest" {
    output = "manifest.json"
   }
}