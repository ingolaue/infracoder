build {
  sources = ["source.oracle-oci.phoenix"]

  provisioner "shell" {
    inline = [
      "sleep 3",
      "sudo -u root echo 'AlfrescoCommunity' > .ImageID"
    ]
  }
  provisioner "file" {
    source = "../../bash/alfresco_ol77.sh"
    destination = "/tmp/alfresco_ol77.sh"
  }
  provisioner "file" {
    source = "credentials.txt"
    destination = "/tmp/credentials.txt"
  }
  provisioner "shell" {
    inline = [
      "echo 'changeme' | sudo chmod +x /tmp/alfresco_ol77.sh",
      "sudo /tmp/alfresco_ol77.sh"
    ]
  }

  post-processor "manifest" {
    output = "manifest.json"
   }
}