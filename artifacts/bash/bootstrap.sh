
#!/bin/bash

## --- Header ---
# Prepared for Oracle
# Service: Management Network
# Description: Create an own iP address space for management purposes
# Web: https://docs.cloud.oracle.com/iaas/Content/Network/Tasks/managingVCNs.htm?Highlight=VCN
# Base image: none
# Interface: none
# Version: 0.1
# Date: 02/01/2020
# Author: torsten.boettjer@oracle.com
# Tags: [MGT, Admin, VCN]

## --- Variables ---
cmprt="ocid1.compartment.oc1..xxx"
location="us-phoenix-1"

# Ingress rules
portlist='22 80 443'
declare -a ingports

for port in $portlist; do
ingress () {
cat <<EOF
{"source": "0.0.0.0/0", "source-type": "CIDR_BLOCK", "protocol": 6, "isStateless": false, "tcp-options": {"destination-port-range": {"max": $port, "min": $port}}}
EOF
};
ingports+=$(ingress);
done

ingsecrules=$(echo ${ingports[@]} | jq . -s);

# Egress rules
egsecrule='{"destination": "0.0.0.0/0", "destination-type": "CIDR_BLOCK", "protocol": "all", "isStateless": false}'


## --- Virtual Cloud Network ---
echo "-- create the MGT VCN"
export vcn=$(oci network vcn create --cidr-block 10.0.0.0/24 -c $cmprt --display-name MGT --dns-label mgt --region $location | jq --raw-output .data.id)

## --- MGT Subnet ---
echo "-- create ADM subnet and associated security list"
sl=$(oci network security-list create --display-name MGT_SL --vcn-id $vcn -c $cmprt --egress-security-rules "[ $egsecrule ]" --ingress-security-rules "$ingsecrules" | jq --compact-output [.data.id])
export sbntmgt=$(oci network subnet create --cidr-block 10.0.0.0/25 -c $cmprt --display-name ADM --vcn-id $vcn --security-list-ids $sl | jq --raw-output .data.id)

## --- Create State-File Bucket ---
ns=$(oci iam tag-namespace create -c $cmprt --name tfstate --description "Bucket for Terraform state-file" | jq .data[] | jq .id -r)
oci iam tag create --name "phase" --description "Resource Management" --tag-namespace-id $ns
oci os bucket create -c $cmprt --name tfstate --defined-tags '{"MGT": {"phase": "fulfillment"}}'

## --- Enabling Communication ---
echo "-- create the Internet Gateway then add a route rule to the default route table used by both subnets"
igw=$(oci network internet-gateway create -c $cmprt --is-enabled true --vcn-id $vcn --display-name MGT_IGW | jq --compact-output '. | [{cidrBlock:"0.0.0.0/0",networkEntityId: .data.id}]')
rt=$(oci network route-table list -c $cmprt --vcn-id $vcn | jq --raw-output '.data[].id')
oci network route-table update --rt-id $rt --route-rules $igw --force

# --- Launch a Server Instance ---
# echo "-- launch a server instance "
# first get the name of AD1
# export ad=$(oci iam availability-domain list -c $cmprt | jq --raw-output .data[0].name)
# export image="ocid1.image.oc1..xxx"
# export shape="VM.Standard.E2.2"
# export sshkey=$(cat "~/.ssh/id_rsa.pub")
# export compute=$(oci compute instance launch -c $cmprt --availability-domain $ad --shape $shape --display-name "AdminHost" --image-id $image --subnet-id $sbntmgt --assign-public-ip true --metadata "{\"ssh_authorized_keys\": \"$sshkey\"}" | jq --raw-output .data.id)
