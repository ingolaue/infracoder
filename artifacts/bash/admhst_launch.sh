#!/bin/bash

## --- Header ---
# Prepared for Oracle
# Service: Deployment Testing
# Description: Test the successfull deployment of the AdminHost
# Web: https://cockpit-project.org/
# Base image: Oracle Linux 7.7
# Interface: UI, https, Port 443
# Version: 0.1 
# Date: 02/01/2020
# Author: torsten.boettjer@oracle.com
# Tags: [MGT, Admin, VCN]

## --- Variables ---
cmprt=$(oci iam compartment list --all --query 'data[].{Name: "name", OCID: "id"}' | jq -r '.[] | select(.Name=="MGT") | .OCID')
vcn=$(oci network vcn list -c $cmprt | jq -r '.data[] | select(."display-name"=="MGT") | .id')
sbnt=$(oci network subnet list -c $cmprt --vcn-id $vcn | jq -r '.data[] | select(."display-name"=="ADM") | .id')
ad=$(oci iam availability-domain list -c $cmprt | jq -r '.data[0] | .name')
img=$(oci compute image list -c $cmprt | jq -r '.data[] | select(."display-name"=="AdminHost") | .id')
shp="VM.Standard2.1"
sshkey=$(cat "$HOME/.ssh/id_rsa.pub")

## --- Launch Server Instance ---
oci compute instance launch -c $cmprt --availability-domain $ad --shape $shp --display-name "AdminHost" --image-id $img --subnet-id $sbnt --assign-public-ip true --metadata "{\"ssh_authorized_keys\": \"$sshkey\"}"

## --- Retrieve Public IP ---
clear && echo "This will take a moment ..."
sleep 2m
admhst=$(oci compute instance list -c $cmprt | jq -r '.data[] | select(."display-name"=="AdminHost") | select(."lifecycle-state"=="RUNNING") | .id')
ip=$(oci compute instance list-vnics --instance-id $admhst | jq -r '.data[] | ."public-ip"')

## --- Output ---
echo "$(echo 'https://')"$ip"$(echo '/')"