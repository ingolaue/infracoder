#!/bin/bash

## --- Header ---
# Prepared for Oracle
# Service: Cockpit
# Description: Administratiion Host - expose ssh terminal to a browser via HTTPS
# Web: https://cockpit-project.org/
# Base image: Oracle Linux 7.7
# Interface: <URL>
# Version: 0.4 
# Date: 02/01/2020
# Author: torsten.boettjer@oracle.com
# Tags: [MGT]

## --- Variables ---
export LANGUAGE=$(echo $LANG)
export LC_ALL=$(echo $LANG)

TerraformRelease="0.12.18"
PackerRelease="1.5.1"

username=$(awk '{print $1}' /tmp/credentials.txt)
password=$(awk '{print $2}' /tmp/credentials.txt)

PortList='22 80 443'

## --- System Updates ---
sudo yum clean all
sudo yum makecache fast
sudo yum -y update
sudo yum -y install jq wget unzip cockpit policycoreutils-python oci-utils nmap python3

## --- Admin User ---
clear

if [ $(id -u) -eq 0 ]; then
	egrep "^$username" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo "$username exists!"
		exit 1
	else
		pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
		useradd -m -p $pass $username
		[ $? -eq 0 ] && echo -e "User has been added to system!" || echo -e "Failed to add a user!"
	fi

    sudo usermod -aG wheel $username
    mkdir /home/$username/.ssh && sudo chown $username:$username /home/$username/.ssh/
    
    SSHScrt=/home/opc/.ssh/authorized_keys
    if [ -f "$SSHScrt" ]; then
      sudo cp /home/opc/.ssh/authorized_keys /home/$username/.ssh/authorized_keys && sudo chown $username:$username /home/$username/.ssh/authorized_keys
    else 
      sudo touch /home/$username/.ssh/authorized_keys && sudo chown $username:$username /home/$username/.ssh/authorized_keys
    fi

    sudo chmod 700 /home/$username/.ssh/ && sudo chmod 600 /home/$username/.ssh/authorized_keys
    sudo echo "AllowUsers $username" >> /etc/ssh/sshd_config
    sudo restorecon -R -v /home/$username/.ssh

    # Install CLI "PATH=$PATH:</home/$username/bin/oci>" is appended to /home/$username/.bashrc
    cd /home/$username
    wget https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh 
    sudo chmod +x install.sh
    sudo chown $username:$username /home/$username/install.sh 
    sudo -H -u $username bash -c './install.sh --accept-all-defaults'

    # Install git and clone infracoder repository
    sudo yum -y install git
    sudo git clone https://gitlab.com/tboettjer/infracoder.git /home/$username/infracoder/
    sudo chown $username:$username /home/$username/infracoder/
    sudo rm -r /home/$username/infracoder/.git && sudo rm -r /home/$username/infracoder/docs
    sudo rm /home/$username/infracoder/*.md && sudo rm /home/$username/infracoder/.gitignore
    touch /home/$username/infracoder/README.md
    sudo chown -R $username:$username /home/$username/infracoder 
else
	echo "Only root may add a user to the system"
	exit 2
fi

## --- Software Installation ---
cd /tmp
wget https://releases.hashicorp.com/terraform/$TerraformRelease/terraform_${TerraformRelease}_linux_amd64.zip
wget https://releases.hashicorp.com/packer/$PackerRelease/packer_${PackerRelease}_linux_amd64.zip
sudo unzip ./terraform_${TerraformRelease}_linux_amd64.zip -d /usr/local/bin/
sudo unzip packer_${PackerRelease}_linux_amd64.zip -d /usr/local/bin/

## --- Enabling Communication ---
for val in $PortList;
  do
    sudo firewall-cmd --zone=public --add-port=$val/tcp --permanent
  done
sudo firewall-cmd --reload

# Start and enable cockpit
sudo sed -i 's;'9090';'443';' /usr/lib/systemd/system/cockpit.socket
sudo semanage port -m -t websm_port_t -p tcp 443
sudo systemctl enable --now cockpit.socket

## --- Housekeeping ---
# Clean directories
sudo rm /tmp/credentials.txt

# Delete default user 'training'
if id -u "training" >/dev/null 2>&1; then
  sudo userdel -Z -f -r training
  clear
  echo "installation completed, please logout, open the browser and login with the new user"
else
  clear
  echo "installation completed, please logout, open the browser and login with the new user"
fi