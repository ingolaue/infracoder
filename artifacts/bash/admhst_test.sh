#!/bin/bash

## --- Header ---
# Prepared for Oracle
# Service: Deployment Testing
# Description: Test the successfull deployment of the AdminHost
# Web: https://cockpit-project.org/
# Base image: Oracle Linux 7.7
# Interface: none
# Version: 0.1 
# Date: 02/01/2020
# Author: torsten.boettjer@oracle.com
# Tags: [MGT, Admin, VCN]

## --- Variables ---
cmprt=$(oci iam compartment list --all --query 'data[].{Name: "name", OCID: "id"}' | jq -r '.[] | select(.Name=="MGT") | .OCID')
img=$(oci compute image list -c $cmprt | jq -r '.data[] | select(."display-name"=="AdminHost") | .id')

## --- Output ---
echo "$(echo 'Image created with OCID ') $img)"