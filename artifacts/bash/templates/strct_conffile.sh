## --- Header ---
<Information regarding the service and the context of use>

## --- Variables ---
<Configuration Elements that are refered in the script>

## --- System Updates ---
<Updating and adding packages to the operating system>

## --- Admin User ---
<Creating a an admin user incl. home directory and bash profile>

## --- Software Installation ---
<Additonal install of software that is not maintained by the package manager>

## --- Enabling Communication ---
<Configuring communication interfaces, the firewall daemon and create SE Linux policies>