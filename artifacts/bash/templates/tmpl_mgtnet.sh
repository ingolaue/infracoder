#!/bin/bash

## --- Header ---
# Prepared for Oracle
# Service: Management Network
# Description: Create an own iP address space for management purposes
# Web: https://docs.cloud.oracle.com/iaas/Content/Network/Tasks/managingVCNs.htm?Highlight=VCN
# Base image: none
# Interface: none
# Version: 0.1 
# Date: 02/01/2020
# Author: torsten.boettjer@oracle.com
# Tags: [MGT, Admin, VCN]

## --- Variables ---
# ingress ports
PortList='21 22 80 443 5910 5911 5912'
# Set the compartment and the location
# cmprt="ocid1.compartment.xxx"
location="us-phoenix-1"
# Create egress rules
egsecrule='{"destination": "0.0.0.0/0", "destination-type": "CIDR_BLOCK", "protocol": "all", "isStateless": false}'
# Create ingress rules
igsecrule22='{"source": "0.0.0.0/0", "source-type": "CIDR_BLOCK", "protocol": 6, "isStateless": false, "tcp-options": {"destination-port-range": {"max": 22, "min": 20}}}'
igsecrule80='{"source": "0.0.0.0/0", "source-type": "CIDR_BLOCK", "protocol": 6, "isStateless": false, "tcp-options": {"destination-port-range": {"max": 80, "min": 80}}}'
igsecrule443='{"source": "0.0.0.0/0", "source-type": "CIDR_BLOCK", "protocol": 6, "isStateless": false, "tcp-options": {"destination-port-range": {"max": 443, "min": 443}}}'

## --- Virtual Cloud Network ---
echo "-- create the MGT VCN"
export vcn=$(oci network vcn create --cidr-block 10.0.0.0/24 -c $cmprt --display-name MGT --dns-label mgt --region $location | jq --raw-output .data.id)

## --- Administration Subnet ---
echo "-- create ADMN subnet with its associated security list"
slmgmt=$(oci network security-list create --display-name MGT_ADMN --vcn-id $vcn -c $cmprt --egress-security-rules "[ $egsecrule ]" --ingress-security-rules "[ $igsecrule22,$igsecrule80,$igsecrule443 ]" | jq --compact-output [.data.id])
export sbntmgmt=$(oci network subnet create --cidr-block 10.0.0.0/25 -c $cmprt --display-name MGT_ADMN --vcn-id $vcn --security-list-ids $slmgmt | jq --raw-output .data.id)

## --- Artifact Subnet ---
echo "-- create lNCH subnet with its associated security list"
sl=$(oci network security-list create --display-name MGT_LNCH --vcn-id $vcn -c $cmprt --egress-security-rules "[ $egsecrule ]" --ingress-security-rules "[ $igsecrule22,$igsecrule80,$igsecrule443 ]" | jq --compact-output [.data.id])
export sbnt=$(oci network subnet create --cidr-block 10.0.0.128/28 -c $cmprt --display-name MGT_LNCH --vcn-id $vcn --security-list-ids $sl | jq --raw-output .data.id)

## --- Enabling Communication ---
echo "-- create the Internet Gateway then add a route rule to the default route table used by both subnets"
igw=$(oci network internet-gateway create -c $cmprt --is-enabled true --vcn-id $vcn --display-name MGT_IGW | jq --compact-output '. | [{cidrBlock:"0.0.0.0/0",networkEntityId: .data.id}]')
rt=$(oci network route-table list -c $cmprt --vcn-id $vcn | jq --raw-output '.data[].id')
oci network route-table update --rt-id $rt --route-rules $igw --force

# --- Optional: Launch Test-Server ---
echo "-- create a compute instance from Oracle Linux 8 image"
# first get the name of AD1
export ad=$(oci iam availability-domain list -c $cmprt | jq --raw-output .data[0].name)
export ol8image="ocid1.image.oc1..aaaaaaaaiqhm2r5wsdd4p2dnz4s2q34nblvxwui45v6vpegpr2rjfeqb7r7q"
export shape="VM.Standard.E2.2"
export sshkey=$(cat "$HOME/.ssh/id_rsa.pub")
export compute=$(oci compute instance launch -c $cmprt --availability-domain $ad --shape $shape --display-name "admhost" --image-id $ol8image --subnet-id $sbntmgmt --assign-public-ip true --metadata "{\"ssh_authorized_keys\": \"$sshkey\"}" | jq --raw-output .data.id)