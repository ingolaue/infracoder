#!/bin/bash

## --- Header ---
## Prepared for <organization>
## Service: <name>
## Description: <description>
## Based on: <Public Website>
## Base image: Oracle Linux 7.7
## Interface: <URL>
## Version: <0.0> 
## Date: <date>
## Author: <eMail>
## Tags: [...]


## --- Variables ---
# Enivronment variables
<Environmental varaibles>

# Shell varaibles
<Shell varaibles>

# Inputs
username=${1?Error: no name given}
password=${2?Error: no password given}

# Lists
PortList='22 80 443 990 1521'

## --- System Updates ---
sudo yum clean all
sudo yum -y update
sudo yum -y install jq wget unzip nmap oci-utils <...>

## --- Admin User ---
if [ $(id -u) -eq 0 ]; then
	egrep "^$username" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo "$username exists!"
		exit 1
	else
		pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
		useradd -m -p $pass $username
		[ $? -eq 0 ] && echo -e "User has been added to system!" || echo -e "Failed to add a user!"
	fi
    sudo usermod -aG wheel $username
    mkdir /home/$username/.ssh && sudo chown $username:$username /home/$username/.ssh/
    
    SSHScrt=/home/opc/.ssh/authorized_keys
    if [ -f "$SSHScrt" ]; then
      sudo cp /home/opc/.ssh/authorized_keys /home/$username/.ssh/authorized_keys && sudo chown $username:$username /home/$username/.ssh/authorized_keys
    else 
      sudo touch /home/$username/.ssh/authorized_keys && sudo chown $username:$username /home/$username/.ssh/authorized_keys
    fi

    sudo chmod 700 /home/$username/.ssh/ && sudo chmod 600 /home/$username/.ssh/authorized_keys
    sudo echo "AllowUsers $username" >> /etc/ssh/sshd_config
    sudo restorecon -R -v /home/$username/.ssh
else
	echo "Only root may add a user to the system"
	exit 2
fi


## --- Software Installation ---
# Download and decompress additional software
cd /tmp
wget <url>
sudo unzip <filename> -d <target directory>
<Install procedure>
# Customize configuration files
<Use SED to customize configuration files>

## --- Enabling Communication ---
# Configure local security system
for val in $PortList;
  do
    sudo firewall-cmd --zone=public --add-port=$val/tcp --permanent
    sudo semanage port -m -t websm_port_t -p tcp $val
  done
sudo firewall-cmd --reload

# Start and enable <service>
sudo systemctl enable --now <services>

# Disable default ssh login
if id -u "training" >/dev/null 2>&1; then
  sudo userdel -Z -f -r training
  clear
  echo "installation completed, please logout and login with the new user"
else
  clear
  echo "installation completed, please logout and login with the new user""
fi