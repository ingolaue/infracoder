#!/bin/bash

## --- Header ---
<Information regarding the service and the context of use>

## --- Variables ---
<Configuration Elements that are refered in the script>

## --- Virtual Cloud Network ---
<Create an own iP address space for management purposes>

## --- Administration Subnet ---
<Prepare the launch of management services in OCI>

## --- Artifact Subnet ---
<Carve out a small zone for the creation of artifacts>

## --- Enabling Cumminication ---
<Deploying an Internet-Gateway to provision public IP>