#!/bin/bash

## --- Header ---
# Prepared for Bayrische Justiz
# Service: Alfresco
# Description: Document Management Server
# Web: https://www.alfresco.com/
# Base image: Oracle Linux 7.7
# Interface: <URL>
# Version: 0.1 
# Date: 13/01/2020
# Author: torsten.boettjer@oracle.com
# Tags: [MGT]

## --- Variables ---
export LC_ALL=$(echo $LANG)

username=$(awk '{print $1}' /tmp/credentials.txt)
password=$(awk '{print $2}' /tmp/credentials.txt)

PortList='22 80 443 8080'

## --- System Updates ---
sudo yum makecache fast
sudo yum clean all
sudo yum -y update
sudo yum -y install fontconfig libSM libICE libXrender libXext cups-libs libGLU cairo mesa-libGL-devel python3 wget policycoreutils-python 
sudo yum -y remove postfix

## --- Admin User ---
if [ $(id -u) -eq 0 ]; then
	egrep "^$username" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo "$username exists!"
		exit 1
	else
		pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
		useradd -m -p $pass $username
		[ $? -eq 0 ] && echo -e "User has been added to system!" || echo -e "Failed to add a user!"
	fi

    sudo usermod -aG wheel $username
    mkdir /home/$username/.ssh && sudo chown $username:$username /home/$username/.ssh/
    
    SSHScrt=/home/opc/.ssh/authorized_keys
    if [ -f "$SSHScrt" ]; then
      sudo cp /home/opc/.ssh/authorized_keys /home/$username/.ssh/authorized_keys && sudo chown $username:$username /home/$username/.ssh/authorized_keys
    else 
      sudo touch /home/$username/.ssh/authorized_keys && sudo chown $username:$username /home/$username/.ssh/authorized_keys
    fi

    sudo chmod 700 /home/$username/.ssh/ && sudo chmod 600 /home/$username/.ssh/authorized_keys
    sudo echo "AllowUsers $username" >> /etc/ssh/sshd_config
    sudo restorecon -R -v /home/$username/.ssh

    # Install git and clone infracoder repository
    sudo yum -y install git
    sudo git clone https://gitlab.com/tboettjer/infracoder.git /home/$username/git/
    sudo chown $username:$username /home/$username/git/
    sudo rm -r /home/$username/git/.git && sudo rm -r /home/$username/git/docs
    sudo rm /home/$username/git/*.md && sudo rm /home/$username/git/.gitignore
    touch /home/$username/git/README.md
    sudo chown -R $username:$username /home/$username/git 
else
	echo "Only root may add a user to the system"
	exit 2
fi

## --- Software Installation ---
cd /tmp
wget https://download.alfresco.com/release/community/201707-build-00028/alfresco-community-installer-201707-linux-x64.bin
sudo chmod +x alfresco-community-installer-201707-linux-x64.bin
sudo cp /home/$username/git/artifacts/bash/alfresco_install_opts /tmp/alfresco_install_opts
sudo ./alfresco-community-installer-201707-linux-x64.bin --optionfile alfresco_install_opts

## --- Enabling Communication ---
for val in $PortList;
  do
    sudo firewall-cmd --zone=public --add-port=$val/tcp --permanent
  done

sudo firewall-cmd --reload

## --- Start Alfresco ---
sudo semanage port -m -t http_port_t -p tcp 8080
sudo systemctl start alfresco

## --- Housekeeping ---
# Clean directories
sudo rm /tmp/credentials.txt

# Delete default user 'training'
if id -u "training" >/dev/null 2>&1; then
  sudo userdel -Z -f -r training
  echo "installation completed, please logout"
else
  clear
  echo "installation completed, please logout"
fi