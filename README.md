# Getting started with "Infrastructure as Code" on Oracle Cloud Infrastructure (OCI)

[Oracle Cloud Infrastruture (OCI)](https://www.oracle.com/cloud/) is a second generation infrastruture service that combines the self-service of a cloud with the control of physical infrastructure. OCI is a network of global datacenters that allows enterprises to obtain resources on-demand and combine predefined services with individual solutions in an own, physically isolated network. Solution deployments are autmated through a single API, facilitating the build process for virtual data centers that extend on-prem infrastructure and allow to manage business critical data within a digital ecosystems. OCI offers the complete range of low-level components such as compute, storage and network in form of dedicated infrastructure. Logical resources such as hypervisors, orchestrators, network functions and higher level services like databases or container orchestrators are offered either as managed services or as blueprints. The construct of compartments offers operators a choice, whether they want to rely on a subscription services, out-task service management or manage and maintain services themselfes. Independent from the delivery model, operators always remain in control over their resources. A [Getting Started Guide](http://bit.ly/2rujPXT) intrduces most of the resources and services offered by Oracle Cloud Infrastructure. 

[<img src="docs/process.png">](http://bit.ly/2pJJn2F)

Infrastructure coding is an effective way to manage OCI resources and services. We define infrastructure with machine readable files. Server configurations are captured in procedural scripts, transformed into artifacts (images or container) and merged into solution topologies. While a [Command Line Interface (CLI)](https://docs.cloud.oracle.com/iaas/Content/API/Concepts/cliconcepts.htm) allows to execute commands interactively, configuration templates foster standardization and reusability accross regions, datacenters and host types. The following sequence helps to get started with infrastructure coding on OCI:

1.  [Server Configuration](bash.md)
2.  [Code Sharing](git.md)
3.  [Bootstrap a Tenant](cli.md)
4.  [Creating Custom Images](packer.md)
5.  [Topology Definition](terraform.md)

Splitting the codebase in configuration-, artifact- and topology-definitions enables continuous deployments with a separation of concerns between application and infrastructure management. Configuration files are developed in close collaboration with application managers, artifact and topology templates enable infrastructure operators to maintain a standardized set of monitoring and management tools.

[<img src="docs/curriculum.png">](http://bit.ly/2pJJn2F)

We rely on [bash](https://learning.oreilly.com/videos/linux-shell-scripting/9781789800906) to write configuration files, with [OCI utilities](https://docs.cloud.oracle.com/iaas/Content/Compute/References/ociutilities.htm) and streaming editors like *SED* and *jq* enhancing the operating system. For templates we employ HCL, an open source language developed for tools like [Packer](https://www.packer.io/) and [Terraform](https://www.terraform.io). An own OCI [Service Provider](https://github.com/terraform-providers/terraform-provider-oci) translates templates into API calls that configure, add, change or delete resources and services.  While Packer helps automation engineers to create artifacts, Terraform is used to define topologies that combine custom- with service provider artifacts. In case of a topology change, Terraform determines deviations from the current state and executes incremental plans.

### Coding Environment

In order to get started we need access to an OCI tenant, the easiest way to obtain a tenant is **signing up for [Oracle's Free Tier](https://www.oracle.com/cloud/free/)**. We recommend a modular editor like [Code](https://code.visualstudio.com/download) for the development of infrastructure code. Modules and plugins like the [Terraform](https://marketplace.visualstudio.com/items?itemName=mauve.terraform) plugin extend the base functionality with linting and auto-completion for HCL and bash code. Using [Git](https://git-scm.com/downloads/guis) and storing the state-file in the object store allows infrastructure coders to work as a team. In addition to that, the OCI [Resource Manager](https://docs.cloud.oracle.com/iaas/Content/ResourceManager/Concepts/resourcemanager.htm) enables engineers to share Terraform templates for execution. The complete list of InfraCoder tools contains:

*  [Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html) for topology deployments
*  [Packer](https://www.packer.io/intro/getting-started/install.html) to create custom artifacts
*  [OCI-CLI](https://docs.cloud.oracle.com/iaas/Content/API/SDKDocs/cliinstall.htm) to accelerate template development
*  [Git](https://git-scm.com) for version control and code distribution
*  [jq](https://stedolan.github.io/jq/) as JSON parser

Instead of installing the entire toolset, we recommnend to download and use the [InfraCoder Image](http://bit.ly/2EPHgyb). It requires [VirtualBox](https://www.virtualbox.org/wiki/Downloads) though, but contains all tools, templates and modules that we reference in this tutorial. 

[>>](bash.md)